import { Component } from "react";

import HeaderText from "./header-text/HeaderText";
import HeaderImage from "./header-image/HeaderImage";

class Header extends Component {
    render() {
        return (
            <>
                <HeaderText />
                <HeaderImage />
            </>
        )
    }
}

export default Header;
