import { Component } from "react";

import background from "../../../assets/images/background.jpg";

class HeaderImage extends Component {
    render() {
        return (
            <div className="row mt-3">
                <img alt="background" src={background} width="800px"/>
            </div>
        )
    }
}

export default HeaderImage;
