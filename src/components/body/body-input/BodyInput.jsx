import { Component } from "react";

class BodyInput extends Component {

    onInputChangeHandler = (event) => {
        console.log("Ô input được nhập");

        console.log(event.target.value);

        this.props.updateInputMessageProp(event.target.value);
    }

    onButtonClickHandler = () => {
        console.log("Nút gửi thông điệp được bấm");

        this.props.updateOutputProp();
    }

    render() {
        return (
            <>
                <div className="row mt-4">
                    <label className="form-label">Message cho bạn 12 tháng tới:</label>
                    <input 
                        placeholder="Nhập message của bạn vào đây" 
                        className="form-control" 
                        onChange={this.onInputChangeHandler}
                        value={this.props.inputMessageProp}
                    ></input>
                </div>
                <div className="row mt-3">
                    <button className="btn btn-success m-auto" style={{width: "150px"}} onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                </div>
            </>
        )
    }
}

export default BodyInput;
