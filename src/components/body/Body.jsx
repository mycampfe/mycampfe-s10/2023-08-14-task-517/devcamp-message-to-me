import { Component } from "react";
import BodyInput from "./body-input/BodyInput";
import BodyOutput from "./body-output/BodyOutput";

class Body extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }

    // Tạo hàm cho phép update inputMessage
    updateInputMessage = (message) => {
        this.setState({
            inputMessage: message
        })
    }

    updateOuput = () => {
        this.setState({
            outputMessage: [...this.state.outputMessage, this.state.inputMessage],
            likeDisplay: true
        })
    }

    render() {
        return (
            <>
                <BodyInput 
                    inputMessageProp={this.state.inputMessage} 
                    updateInputMessageProp = {this.updateInputMessage}
                    updateOutputProp={this.updateOuput}
                />

                <BodyOutput 
                    outputMessageProp={this.state.outputMessage}
                    likeDisplayProp={this.state.likeDisplay}
                />
            </>
        )
    }
}

export default Body;
